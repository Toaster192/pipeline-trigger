"""
Trigger for Brew/Koji builds using UMB. WIP.
See https://pagure.io/fedora-ci/messages for details.
"""
import copy
import json
import logging
import os
import re
import subprocess

import proton
import proton.handlers
from proton.reactor import Container

from . import utils


BREW = ['brew', '--noauth']


class Receiver(proton.handlers.MessagingHandler):
    def __init__(self, cert_path, urls, topics, gitlab_instance, trigger_token,
                 triggers):
        super(Receiver, self).__init__()

        self.cert_path = cert_path
        self.urls = urls
        self.topics = topics
        self.gitlab_instance = gitlab_instance
        self.trigger_token = trigger_token
        self.triggers = triggers

    def on_start(self, event):
        ssl = proton.SSLDomain(proton.SSLDomain.MODE_CLIENT)
        ssl.set_credentials(self.cert_path, self.cert_path, None)
        ssl.set_trusted_ca_db('/etc/ssl/certs/ca-bundle.crt')
        ssl.set_peer_authentication(proton.SSLDomain.VERIFY_PEER)
        conn = event.container.connect(urls=self.urls, ssl_domain=ssl)

        for topic in self.topics:
            event.container.create_receiver(conn, source=topic)

    def on_message(self, event):
        message = json.loads(event.message.body)
        properties = event.message.properties

        # Sanity check
        if properties.get('new') != 'CLOSED' and \
                properties.get('attribute') != 'state':
            logging.error('Unexpected task.closed message: %s', properties)
            return

        # We are only interested in completed builds
        if properties.get('method') != 'build':
            return

        task_id = message['info']['id']
        logging.debug('A build with task ID %s completed!', task_id)

        # Get the package name from the task request
        try:
            request_string = message['info']['request'][0]
        except (IndexError, KeyError):
            logging.info('Task %s is not a build request, ignoring', task_id)
            return

        rpm_match = re.search(r'/rpms/(kernel)(.git)?\??#', request_string)
        if rpm_match:
            logging.info('Kernel build from dist-git found!')
            # These don't contain full NVR in the request, just "kernel", and
            # we need to get it from the task itself.
            task_data = subprocess.check_output(
                BREW + ['taskinfo', str(task_id)]
            ).decode()
            nvr_match = re.search(r'Build: (kernel-.*) ', task_data)
            if not nvr_match:
                logging.error(
                    'Unexpectedly can\'t find kernel NVR in task data: %s',
                    task_data
                )
                return

            nvr = nvr_match.group(1)
        else:
            nvr_match = re.search(r'(kernel-.*\.rpm)', request_string)
            if not nvr_match:
                logging.debug('Non-kernel build request detected: %s',
                              request_string)
                return
            nvr = nvr_match.group(1)

        logging.info('Kernel build for %s detected!', nvr)
        release_match = re.search(r'kernel-(?:[\d]+[.-])+([a-z\d]+)', nvr)
        if not release_match:
            logging.error('Can\'t get OS release from %s!', nvr)
            return

        release = release_match.group(1)
        trigger_to_use = next((trigger for trigger in self.triggers
                               if trigger['rpm_release'] == release),
                              None)
        if not trigger_to_use:
            logging.debug('Pipeline for %s not configured', release)
            return

        trigger_to_use['nvr'] = nvr
        trigger_to_use['brew_task_id'] = str(task_id)
        trigger_to_use['title'] = 'Brew: Task {}'.format(task_id)
        utils.trigger_pipelines(self.gitlab_instance,
                                self.trigger_token,
                                [trigger_to_use])

    def on_link_opened(self, event):
        logging.info('Link opened to %s at address %s',
                     event.connection.hostname,
                     event.link.source.address)

    def on_link_error(self, event):
        logging.error('Link error: %s: %s',
                      event.link.remote_condition.name,
                      event.link.remote_condition.description)
        logging.info('Closing connection to %s', event.connection.hostname)
        event.connection.close()

    def on_transport_error(self, event):
        """
        Called when some error is encountered with the transport over which the
        AMQP connection is to be established. This includes authentication
        errors as well as socket errors.
        """
        logging.error('Transport error: %s', event)
        if event.transport.condition:
            condition = event.transport.condition
            if event.transport.condition.name in self.fatal_conditions:
                event.connection.close()
                raise Exception('Fatal transport error: {}: {}'.format(
                    condition.name,
                    condition.description
                ))
        else:
            raise Exception('Unspecified transport error!')


def poll_triggers(gitlab_instance, brew_config, gitlab_session, trigger_token):
    # Create the actual triggers and start the receiver
    triggers = []
    for key, value in brew_config['pipelines'].items():
        trigger = copy.deepcopy(brew_config)
        trigger['name'] = key
        for pipeline_key, pipeline_value in value.items():
            trigger[pipeline_key] = pipeline_value
        del trigger['pipelines']

        trigger['send_report_to_upstream'] = str(
            trigger.get('send_report_to_upstream', False)
        )
        trigger['send_report_on_success'] = str(
            trigger.get('send_report_on_success', False)
        )

        del trigger['receiver_urls']
        del trigger['cert_path']
        del trigger['message_topics']

        triggers.append(trigger)

    receiver = Container(Receiver(
        brew_config['cert_path'],
        brew_config['receiver_urls'],
        brew_config['message_topics'],
        gitlab_instance,
        trigger_token,
        triggers
    )).run()
