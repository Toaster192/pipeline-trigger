"""Tests for triggers.cki_github."""
import unittest

import mock

import triggers.cki_github as cki_github

import fakes


class TestCKIGithub(unittest.TestCase):
    """Tests for triggers.cki_github"""
    def test_generate_hash(self):
        """
        Verify generate_hash() returns expected hash if all arguments are
        nonempty.
        """
        github_project_path, pr_id, message_id = 'project/repo', '23', '984'
        self.assertEqual(
            '7058975511d2e26b4c92f5e1dbf0e27bc57d3b14e4a394386e23b4ccd077d750',
            cki_github.generate_hash(github_project_path, pr_id, message_id)
        )

    def test_is_pr_already_reviewed(self):
        """
        Verify is_pr_already_reviewed returns True if PR was already reviewed.
        """
        pull_m = mock.Mock()
        comment_m = mock.Mock()
        comment_m.html_url = 'somethingtomatch'
        review_m = mock.Mock()
        pull_m.get_reviews.return_value = [review_m]
        review_m.body = 'Foo bar somethingtomatch baz'
        self.assertTrue(cki_github.is_pr_already_reviewed(pull_m, comment_m))

        review_m.body = 'Foo bar baz'
        self.assertFalse(cki_github.is_pr_already_reviewed(pull_m, comment_m))

    @mock.patch('triggers.cki_github.is_pr_already_reviewed')
    def test_update_pull_request(self, is_pr_already_reviewed_mock):
        """
        Check if the review is created with the proper event type.
        """
        pull_m = mock.Mock()
        comment_m = mock.Mock()

        is_pr_already_reviewed_mock.return_value = True
        cki_github.update_pull_request(pull_m, comment_m, "failed", [])
        pull_m.create_review.assert_not_called()

        is_pr_already_reviewed_mock.return_value = False
        cki_github.update_pull_request(pull_m, comment_m, "passed", [])
        self.assertEqual(
            'APPROVE',
            pull_m.create_review.call_args[1]['event'],
        )
        cki_github.update_pull_request(pull_m, comment_m, "failed", [])
        self.assertEqual(
            'REQUEST_CHANGES',
            pull_m.create_review.call_args[1]['event'],
        )
