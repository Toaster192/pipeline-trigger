import argparse
import copy
import os
import unittest

import mock

import triggers.__main__ as main_module
import triggers.utils

import fakes


class TestMain(unittest.TestCase):
    """Tests for triggers.__main__.main()."""
    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='test')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('triggers.__main__.trigger_pipelines')
    def test_main_calls(self, mock_trigger, mock_argparse, mock_file,
                        mock_gitlab):
        """Verify main() calls all the functions it should."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='fake-trigger',
            config=None,
            token='GITLAB_TRIGGER_TOKEN',
            login=False,
            kickstart=False
        )
        mock_trigger.return_value = 'something'

        os.environ.update({'GITLAB_URL': 'http://gitlab.test',
                           'GITLAB_PRIVATE_TOKEN': 'private-token',
                           'GITLAB_TRIGGER_TOKEN': 'trigger-token'})

        main_module.main()

        mock_trigger.assert_called_once()

        # Cleanup
        del os.environ['GITLAB_URL']
        del os.environ['GITLAB_PRIVATE_TOKEN']
        del os.environ['GITLAB_TRIGGER_TOKEN']

    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('triggers.__main__.log_into_gitlab')
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='test')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('triggers.__main__.trigger_pipelines')
    def test_login_call(self, mock_trigger, mock_argparse, mock_file,
                        mock_gitlab, mock_login):
        """Verify GitLab login is attempted if --login flag is passed."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='fake-trigger',
            config=None,
            token='GITLAB_TRIGGER_TOKEN',
            login=True,
            kickstart=False
        )
        mock_trigger.return_value = 'something'

        os.environ.update({'GITLAB_URL': 'http://gitlab.test',
                           'GITLAB_PRIVATE_TOKEN': 'private-token',
                           'GITLAB_TRIGGER_TOKEN': 'trigger-token',
                           'GITLAB_LOGIN': 'username',
                           'GITLAB_PASSWORD': 'password'})

        main_module.main()

        mock_login.assert_called_once()
        mock_trigger.assert_called_once()

        # Cleanup
        del os.environ['GITLAB_URL']
        del os.environ['GITLAB_PRIVATE_TOKEN']
        del os.environ['GITLAB_TRIGGER_TOKEN']
        del os.environ['GITLAB_LOGIN']
        del os.environ['GITLAB_PASSWORD']

    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('triggers.utils.log_into_gitlab')
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='test')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('triggers.__main__.trigger_pipelines')
    def test_required_envvars(self, mock_trigger, mock_argparse, mock_file,
                              mock_gitlab, mock_login):
        """Verify EnvVarNotSet is raised if a required variable is missing."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='fake-trigger',
            config=None,
            token='GITLAB_TRIGGER_TOKEN',
            login=True,
            kickstart=False
        )
        mock_trigger.return_value = 'something'

        variables = {'GITLAB_URL': 'http://gitlab.test',
                     'GITLAB_PRIVATE_TOKEN': 'private-token',
                     'GITLAB_TRIGGER_TOKEN': 'trigger-token',
                     'GITLAB_LOGIN': 'username',
                     'GITLAB_PASSWORD': 'password'}

        for variable in variables:
            reduced = copy.deepcopy(variables)
            del reduced[variable]
            with self.assertRaises(triggers.utils.EnvVarNotSetError):
                main_module.main()
